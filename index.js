var express = require('express');
var bodyParser = require('body-parser');
var path =require('path');
const app = express()
var http = require('http');
var https = require('https');
// httpProxy = require('http-proxy')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('*', (req, res) => {
res.send('Hello От Запада')
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.send(500, { message: err.message })
})

// app.listen(80, () => {
//   console.log(`is now up and running on 80`)
// })
https.createServer({rejectUnauthorized: false,requestCert: false}, app).listen(4443);
// httpProxy.createServer({
//   hostnameOnly: true,
//   router: {
//     //web-development.cc
//     'time.mirusdesk.pro': '127.0.0.1:3456',
//     'mirusdesk.pro' : '127.0.0.1:4000'
//   }
// }).listen(80);


http.createServer(function(req, res) {
      // console.log(req.headers,'Handled headers |',req.url)
      res.writeHead(301, {"Location": "https://" + req.headers['host']+':4443' + req.url});
      res.end();
}).listen(8000);
