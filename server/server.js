var express = require('express');
var bodyParser = require('body-parser');
// var mongoose = require('mongoose');
var path =require('path');
const app = express()

// var Post require('./models/post')

app.listen(80, () => {
  console.log(`is now up and running on 80`)
})

var index = require('./routes/index')

// mongoose.connect('mongodb://localhost:27017/SigaProject')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/index', express.static('static/index.html'))
app.use('/icons', express.static('static/icons'))
app.use('/images', express.static('static/images'))
app.use('/font-awesome-4.7.0', express.static('static/font-awesome-4.7.0'))

app.use('/api', index)
// app.use('*', express.static('static/index.html'))
app.get('/style.css', (req, res) => {
res.sendFile('style.css', { root: path.join(__dirname, '../static') });
})
app.get('*', (req, res) => {
res.sendFile('index.html', { root: path.join(__dirname, '../static') });
})
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.send(500, { message: err.message })
})

// Exporting server
// export default { app }
module.exports = app
