var mongoose = require('mongoose');

const Users = mongoose.Schema({
  email: {type: String, require: true, unique: true},
  name: {type: String, require: true, unique: true},
  phone: {type: String, require: true, unique: true},
  password: {type: String, require: true},
  botID: String,
  locations: {type: Array}
  },
  {
    timestamps: true
  })

module.exports = mongoose.model('Users', Users)
